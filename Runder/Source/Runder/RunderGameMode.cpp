// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "RunderGameMode.h"
#include "RunderCharacter.h"
#include "UObject/ConstructorHelpers.h"

ARunderGameMode::ARunderGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/SideScrollerCPP/Blueprints/SideScrollerCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
